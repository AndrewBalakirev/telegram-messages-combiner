FROM python:3.10-alpine

RUN mkdir /bot && cd /bot
WORKDIR /bot

COPY . .
RUN pip install poetry && poetry config virtualenvs.create && poetry install
