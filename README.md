
# Что это?
> Это телеграм клиент, который смотрит, отправляемые вами, сообщения и объединяет их,
> если они были отправлены в промежутке 2-3 секунд

---

# Как запустить?
- Устанвить [docker](https://docs.docker.com/engine/install/ubuntu/) и [docker compose](https://docs.docker.com/compose/install/linux/)
- создать `.env` файл из `.env.dis` и заполнить переменные окружения
- выполнить `docker compose up`
