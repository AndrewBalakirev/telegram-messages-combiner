import os

import uvloop
from pyrogram import filters
from pyrogram.client import Client
from pyrogram.types import Message

from messages_combiner_client.schemas.message import MessageDTO
from messages_combiner_client.services.combiner import MessagesCombiner

uvloop.install()

api_id = os.environ.get("API_ID", "")
api_hash = os.environ.get("API_HASH", "")
app = Client(os.environ.get("SESSION_NAME", ""), api_id, api_hash)


@app.on_message(filters.me)
async def combiner(client: Client, message: Message):
    await MessagesCombiner.combine(MessageDTO(message))


def main():
    app.run()


if __name__ == "__main__":
    main()
