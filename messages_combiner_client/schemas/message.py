from dataclasses import dataclass, field
from datetime import datetime

from pyrogram.types import Message


@dataclass
class MessageDTO:
    tg_message: Message
    id: int = field(init=False)
    chat_id: int = field(init=False)
    text: str = field(init=False)
    date: datetime = field(init=False)
    has_media: bool = field(init=False)
    is_forwarded: bool = field(init=False)
    is_reply: bool = field(init=False)

    def __post_init__(self):
        message = self.tg_message
        self.id = message.id
        self.chat_id = message.chat.id
        self.text = message.text
        self.date = message.edit_date or message.date
        self.has_media = bool(message.media)
        self.is_forwarded = bool(message.forward_date)
        self.is_reply = bool(message.reply_to_message_id)

    async def update_text(self, text: str):
        self.text = text
        await self.tg_message.edit_text(text=self.text)
        self.date = datetime.now()
