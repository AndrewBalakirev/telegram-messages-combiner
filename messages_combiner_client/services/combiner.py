from pyrogram.errors.exceptions import MessageIdInvalid

from messages_combiner_client.schemas.message import MessageDTO


class MessagesCombiner:
    _message: MessageDTO = None
    TIMEOUT: int = 3

    @classmethod
    async def combine(cls, new_message: MessageDTO):
        if new_message.has_media or new_message.is_forwarded:
            # Пропускаем сообщение если оно имеет медию или переслано
            return

        if cls._is_new_chat(new_message) or cls._timeout_is_end(new_message) or new_message.is_reply:
            cls.set_message(new_message)
        else:
            try:
                await cls._combine_message(new_message)
            except MessageIdInvalid:
                MessagesCombiner.set_message(new_message)

    @classmethod
    def set_message(cls, new_message: MessageDTO) -> None:
        cls._message = new_message

    @classmethod
    async def _combine_message(cls, new_message: MessageDTO) -> None:
        await cls._message.update_text(f"{cls._message.text}\n{new_message.text}")
        await new_message.tg_message.delete()

    @classmethod
    def _is_new_chat(cls, message: MessageDTO) -> bool:
        return cls._message is None or cls._message.chat_id != message.chat_id

    @classmethod
    def _timeout_is_end(cls, message: MessageDTO) -> bool:
        time_delta = (message.date - cls._message.date).total_seconds()
        return cls._message is None or time_delta > cls.TIMEOUT
